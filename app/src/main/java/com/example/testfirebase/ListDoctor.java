package com.example.testfirebase;

import java.io.Serializable;
import com.example.testfirebase.ApdapterListDoctor;
import com.example.testfirebase.ListDoctor;

public class ListDoctor implements Serializable {
    private int imgDoctor;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String firstName;
    private String lastName;


    private String hocvi;

    private String address;

    private String schoolname;

    private int graduationYears;

    private String chuyenkhoa;

    private float tienkham;


    public ListDoctor(String firstName, String lastName, String hocvi, String address, String schoolname, int graduationYears, String chuyenkhoa, float tienkham) {
        this.hocvi = hocvi;
        this.address = address;
        this.schoolname = schoolname;
        this.graduationYears = graduationYears;
        this.chuyenkhoa = chuyenkhoa;
        this.tienkham = tienkham;
        this.firstName = firstName;
        this.lastName = lastName;
    }





    public String getHocvi() {
        return hocvi;
    }

    public void setHocvi(String hocvi) {
        this.hocvi = hocvi;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public int getGraduationYears() {
        return graduationYears;
    }

    public void setGraduationYears(int graduationYears) {
        this.graduationYears = graduationYears;
    }

    public String getChuyenkhoa() {
        return chuyenkhoa;
    }

    public void setChuyenkhoa(String chuyenkhoa) {
        this.chuyenkhoa = chuyenkhoa;
    }

    public float getTienkham() {
        return tienkham;
    }

    public void setTienkham(float tienkham) {
        this.tienkham = tienkham;
    }

    public ListDoctor() {
    }
}

