package com.example.testfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.testfirebase.databinding.ActivityHomeBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AddDoctorActivity extends AppCompatActivity {

    ActivityHomeBinding binding;
    TextInputEditText hocvi,diachi,schoolname,namtotnghiep,chuyenkhoa,phikham;
    EditText ho,ten;
    Button btnsend;


    private DatabaseReference reference;
    FirebaseDatabase db;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);
        ho=findViewById(R.id.first_name);
        ten=findViewById(R.id.last_name);
        hocvi=findViewById(R.id.hocvi_text);
        btnsend=findViewById(R.id.btn_addDoctor);
        diachi=findViewById(R.id.addresswork);
        schoolname=findViewById(R.id.schoolname);
        namtotnghiep=findViewById(R.id.namtotnghiep);
        chuyenkhoa=findViewById(R.id.chuyenkhoa);
        phikham=findViewById(R.id.phikham);



        reference=FirebaseDatabase.getInstance().getReference("Doctor");
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNewDoctor();
            }
        });

    }

    private void AddNewDoctor() {
        String Ho =ho.getText().toString();
        String Ten =ten.getText().toString();
        String HocVi=hocvi.getText().toString();
        String DiaChi=diachi.getText().toString();
        String SchoolName=schoolname.getText().toString();
        int NamTotNghiep = Integer.parseInt(namtotnghiep.getText().toString());
        String ChuyenKhoa=chuyenkhoa.getText().toString();
        float phiKham = Float.parseFloat(phikham.getText().toString());

        ListDoctor LvDoctor = new ListDoctor(Ho,Ten,HocVi,DiaChi,SchoolName,NamTotNghiep,ChuyenKhoa,phiKham);

        reference.child(Ten).setValue(LvDoctor);
        Toast.makeText(AddDoctorActivity.this,"Dữ liệu đã được nhập",Toast.LENGTH_LONG).show();
    }


}