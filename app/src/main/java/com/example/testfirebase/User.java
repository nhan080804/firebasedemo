package com.example.testfirebase;
import com.example.testfirebase.ApdapterListDoctor;
import com.example.testfirebase.ListDoctor;

public class User {
    private int Role;
    private String id_User, Address, Email, Gender, FullName, Age, Username, Password;

    public User() {

    }

    public User(int role, String id_User, String address, String email, String gender, String fullName, String age, String username, String password) {
        this.Role = role;
        this.id_User = id_User;
        this.Address = address;
        this.Email = email;
        this.Gender = gender;
        this.FullName = fullName;
        this.Age = age;
        this.Username = username;
        this.Password = password;
    }

    public int getRole() {
        return Role;
    }

    public void setRole(int role) {
        Role = role;
    }

    public String getid_User() {
        return id_User;
    }

    public void setid_User(String id_User) {
        this.id_User = id_User;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}

