package com.example.testfirebase;

import android.content.Intent;
import android.os.Bundle;
import com.example.testfirebase.ApdapterListDoctor;
import com.example.testfirebase.ListDoctor;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingFragment extends Fragment {
    ListView lvDoctor;
    ApdapterListDoctor SpAdapter;

    List<ListDoctor> listDoctorList;
    FirebaseDatabase FireBD;
    DatabaseReference reference;

    androidx.appcompat.widget.AppCompatButton btnaddDoctor;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button btn;


    public BookingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingFragment newInstance(String param1, String param2) {
        BookingFragment fragment = new BookingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking, container, false);
        lvDoctor = view.findViewById(R.id.lvDoctor);
        ShowDoctor();
  /*      addEnvent();*/
        ChuyenTrang(view);
        return view;

    }

  /*  private void addEnvent() {
        btnaddDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AddDoctorActivity.class);
                startActivity(i);
            }
        });
    }*/

    private void ShowDoctor() {
        FireBD=FirebaseDatabase.getInstance();
        reference=FireBD.getReference();
        listDoctorList= new ArrayList<ListDoctor>();
        SpAdapter= new ApdapterListDoctor(getActivity(),R.layout.listdoctorlayout,listDoctorList);
        reference.child("Doctor").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
             for(DataSnapshot ds:snapshot.getChildren()){
                 String FirstNameBS=ds.child("firstName").getValue(String.class);
                 String LasnameBS=ds.child("lastName").getValue(String.class);
                 String hocvi=ds.child("hocvi").getValue(String.class);
                 listDoctorList.add(new ListDoctor(""+FirstNameBS,""+LasnameBS,""+hocvi,"Quan","Huflit",1,"Da Khoa",1));
                 lvDoctor.setAdapter(SpAdapter);
                 SpAdapter.notifyDataSetChanged();
             }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void ChuyenTrang(View view) {
        Intent i = new Intent(getActivity(), AddDoctorActivity.class);
        startActivity(i);
    }
}