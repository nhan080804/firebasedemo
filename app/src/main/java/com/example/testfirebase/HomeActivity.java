package com.example.testfirebase;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.example.testfirebase.ApdapterListDoctor;
import com.example.testfirebase.ListDoctor;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class HomeActivity extends AppCompatActivity {
    BottomNavigationView bnv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        addControl();
        addEvent();

    }
    public void addControl(){
        bnv =findViewById(R.id.bottom_home_main_navigation);
    }
    public void addEvent(){
        bnv.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fm;
                if(item.getItemId()==R.id.btn_nav_booking){
                    fm = new BookingFragment();
                    loadFragment(fm);
                    return true;
                }
                return true;
            }
        });
    }
    public void loadFragment(Fragment fm){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_home,fm,"");
        fragmentTransaction.commit();
    }
}