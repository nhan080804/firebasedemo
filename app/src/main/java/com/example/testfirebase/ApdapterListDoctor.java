package com.example.testfirebase;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.testfirebase.ApdapterListDoctor;
import com.example.testfirebase.ListDoctor;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ApdapterListDoctor extends ArrayAdapter<ListDoctor> {
    Activity context;
    int resource;

    public ApdapterListDoctor(@NonNull Activity context, int resource, @NonNull List<ListDoctor> listDoctor) {
        super(context, resource, listDoctor);
        this.context = context;
        this.resource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = this.context.getLayoutInflater();
        View customView = layoutInflater.inflate(this.resource, null);
        TextView hocvi_text = customView.findViewById(R.id.hocvi_text);
        TextView name_doctor = customView.findViewById(R.id.name_doctor);
        TextView type_chuyenkhoa=customView.findViewById(R.id.chuyenkhoa);
        ListDoctor listDoctor = getItem(position);
        hocvi_text.setText(listDoctor.getHocvi() + "");
        name_doctor.setText(listDoctor.getFirstName() + "");
        type_chuyenkhoa.setText(listDoctor.getChuyenkhoa()+"");
        return customView;
    }
}
